import { SAVE_NEWS } from "../types/hackerNewsTypes";

const initialState = {
    news: []
}

export default function (state = initialState, action) {
    switch (action.type){
        case SAVE_NEWS:
            return { news: action.payload }
        default:
            return { ...state }
    }
}