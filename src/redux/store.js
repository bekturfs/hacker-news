import { createStore, applyMiddleware, compose } from 'redux'
import hackerNewsReducer from './reducers/hackerNewsReducer';
import thunk from "redux-thunk";

const initialState = {};

const store = createStore(
    hackerNewsReducer,
    initialState,
    compose(
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        // applyMiddleware(thunk)
    )
);

export default store;