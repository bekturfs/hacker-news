import {SAVE_NEWS} from "../types/hackerNewsTypes";

export const saveNews = news => ({
    type: SAVE_NEWS,
    payload: news
})