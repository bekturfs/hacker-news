import React from 'react';
import './App.css';
import {hackerNewsServiceContext} from "./contexts";
import HackerNewsService from "./services/hackerNewsService";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Provider} from 'react-redux';
import store from "./redux/store";
import Main from "./pages/Main";
import News from "./pages/News";

const App = () => {
  return (
      <Router>
        <hackerNewsServiceContext.Provider value={new HackerNewsService()}>
          <Provider store={store}>
              <Switch>
                  <Route exact path="/" component={Main} />
                  <Route exact path="/news/:id" component={News} />
              </Switch>
          </Provider>
        </hackerNewsServiceContext.Provider>
      </Router>
  );
}

export default App;
